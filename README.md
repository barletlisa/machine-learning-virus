# Machine Learning Virus
Nous avons divisé ce projet en deux programme, la traitement des données est géré par panda.py (ce qui produit le fichier database), et l'utilisation de ces données pour apprendre est géré dans algo.py.

Notre propgramme (algo.py) utilise différents algorithme avec une taille variable d'échantillon et affiche un graphique des scores de ces algorithmes en fonctions de la tailles des échantillons.

Pour lancer le programme il faut installer:
  - python3
  - pandas
  - pandas_profiling
  - sklearn

Puis lancer la commande "python3 algo.py"

MLPClassifier Multi-layer Perceptron classifier
MLP entraine de manière itérative car à chaque étape la dérivé partiel
de la fonction de perte respectant les paramètre sont calculer et mis à jour


En lançant ce classifier on obtient pas la convergence avec 100 itération en 510 seconde et avec un alpha de 1.
Il est donc beaucoup trop long donc on ne le garde pas.


Nous avons jugé que les résultats cet algorithm était trop lent et puisque d'autre algorithms sont plus appropriés pour faire de la classification binaire.


Les algorithms utilisant des arbres de décisions nous semblent plus adaptés, nous nous somme donc plutôt dirigé vers cette direction.

SVM semblait aussi adapté pour ce genre de problème (binaire), a une complexité trop importante par rapport à l'échantillon, donc nous avons abandonné cette voie.

Nous avons eu nos meilleurs résultat avec Random Forest.

Nous avons joué avec les paramètres afin d'avoir une exécution plutôt rapide (ce qui ne serait pas vraiment un problème en soit, puisque ça n'influe que la durée d'apprentissage) et surtout aussi précis que possible


AdaBoostClassifier
Un classifier qui commence par trier les data des données de départ,
puis fait des copies de ce classifier en ajustant le poids des instances mal classifiées








GaussianNB
utilise le théorème de Naive Bayes basé sur les probabilités conditionnelles pour classifier les données

Le graphique de cet algo montre que le score est bon sur des petits échantillons puis plus l'échantillons augmente, moins le score est bon. On ne va donc pas séléctionner cet algorithme










QuadraticDiscriminantAnalysis
utilise une limite de décision quadratique générée en triant la densité conditionnelle de classe des données et en utilisant la règle de Bayes
