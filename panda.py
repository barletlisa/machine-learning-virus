#_ coding utf-8
import numpy as np
import pandas as pd
import pandas_profiling
import pickle
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_moons, make_circles, make_classification


data = pd.read_csv("antivirus_dataset.csv", sep="|")
#profile = data.profile_report(title='Pandas Profiling Report')
#profile.to_file(output_file="output.html")


# drop columns hightly correlated to other columns
data = data.drop(columns=['SectionsMeanRawsize', 'MinorImageVersion', 'MinorSubsystemVersion', 'SizeOfHeapCommit', 'SectionsMinVirtualsize', 'SizeOfOptionalHeader', 'SectionsMeanVirtualsize'])
# drop columns qui ont plus de 90% du temps la même valeur (0)
data = data.drop(columns=['LoaderFlags', 'SectionAlignment', 'NumberOfRvaAndSizes', 'SizeOfHeapReserve', 'FileAlignment', 'BaseOfCode'])

# drop columns because no correlation expected
data = data.drop(columns=['md5', 'Name'])



# drop others
data = data.drop(columns=['VersionInformationSize',
'SizeOfCode', 'ImportsNbOrdinal',
'ImageBase', 'CheckSum', 'ResourcesMeanSize',
'BaseOfData', 'ImportsNb',
'DllCharacteristics', 'SectionMaxRawsize',
'SectionMaxVirtualsize', 'ResourcesMaxEntropy',
'MinorLinkerVersion', 'SizeOfStackReserve',
'ResourcesMaxSize', 'SizeOfHeaders',
'MajorOperatingSystemVersion', 'ResourcesMeanEntropy',
'ResourcesMinSize', 'MajorLinkerVersion',
'MinorOperatingSystemVersion', 'ImportsNbDLL',
'SizeOfImage', 'LoadConfigurationSize',
'SizeOfStackCommit', 'SectionsMinRawsize',
'SizeOfUninitializedData', 'ResourcesMinEntropy',
'AddressOfEntryPoint', 'MajorImageVersion',
'SectionsMinEntropy', 'ExportNb', 'ExportNb',
'ResourcesNb', 'ResourcesNb', 'SizeOfInitializedData', 'SizeOfInitializedData'])

#profile = data.profile_report(title='Pandas Profiling Report')
#profile.to_file(output_file="output2.html")
print(data.shape)

dbfile = open('database', 'ab')
pickle.dump(data, dbfile)
dbfile.close()
