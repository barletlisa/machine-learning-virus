#_ coding utf-8
import time
import numpy as np
import matplotlib.pyplot as plt
import pickle
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.metrics import mean_squared_error
from sklearn.metrics import explained_variance_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.model_selection import learning_curve
from sklearn.model_selection import ShuffleSplit
from sklearn.datasets import load_digits

dbfile = open('database', 'rb')
data = pickle.load( dbfile)
dbfile.close()


X = data.drop(columns="legitimate")
y = data["legitimate"]

classifiers = [
    #{"clf": KNeighborsClassifier(n_neighbors=5, weights='distance', leaf_size=15),"name": "KNeighborsClassifier"},
    #{"clf": GradientBoostingClassifier(n_estimators=20), "name": "GradientBoostingClassifier"},
    {"clf": DecisionTreeClassifier(max_depth=None, criterion='entropy'), "name": "DecisionTreeClassifier"},
    {"clf": RandomForestClassifier(n_estimators=25, max_depth=20, random_state=0, criterion='entropy'), "name": "RandomForestClassifier"},
    #{"clf": MLPClassifier(alpha=1, max_iter=1000), "name": "MLPClassifier"},
    {"clf": AdaBoostClassifier(), "name": "AdaBoostClassifier"},
    {"clf": GaussianNB(), "name": "GaussianNB"},
    {"clf": QuadraticDiscriminantAnalysis(), "name": "QuadraticDiscriminantAnalysis"}
]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)








for classifier in classifiers :
    debut = time.time()
    print(classifier["name"])
    clf = classifier["clf"]
    clf = clf.fit(X_train, y_train)
    print("temps d'execution : ",  time.time()-debut)

    y_pred = clf.predict(X_test)
    print("mean squarred error", mean_squared_error(y_test, y_pred))
    print("explained variance score",explained_variance_score(y_test, y_pred))
    print("vrai negatif - faux positif")
    print("faux negatif - vrai positif")
    print(confusion_matrix(y_test, y_pred))
    print(classification_report(y_test,y_pred))


    ylim=None
    cv=None
    n_jobs=None
    estimator=classifier["clf"]
    train_sizes=(0.1, 0.2, 0.3, 0.33, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0)


    plt.figure()
    plt.title(classifier["name"])
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")

    plt.show()
